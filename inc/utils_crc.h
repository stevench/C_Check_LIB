#ifndef UTILS_CRC_H
#define UTILS_CRC_H

typedef  unsigned char crc_uint8_t;
typedef  unsigned short crc_uint16_t;
typedef  unsigned int crc_uint32_t;


crc_uint32_t GetCRC32_8bit(crc_uint8_t *pData, crc_uint32_t len);
crc_uint32_t GetCRC32_8bit_Add(crc_uint8_t *pData,crc_uint32_t len,crc_uint32_t BeforeCRC);
crc_uint16_t GetCRC16_8bit(crc_uint8_t *p_uch_Data, crc_uint32_t len );
crc_uint16_t GetCRC16_8bit_Add(crc_uint8_t *pData, crc_uint32_t len,crc_uint16_t BeforeCRC);
#endif //UTILS_CRC_H
