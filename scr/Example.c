#include "utils_md5.h"
#include "utils_sha1.h"
#include "utils_hmac.h"
#include "utils_crc.h"


void example_main()
{
    unsigned char inBuffer[10]="0123456789";
    unsigned char output[40];
    unsigned char Key[5]="12345";
    unsigned char i;
   
    
    uint32_t CRC32=0;
    uint16_t CRC16=0;

    //计算CRC32
    CRC32 = GetCRC32_8bit(inBuffer,10);
    printf("crc32=%x\r\n",CRC32);
    
    
    //追加方式计算CRC32
    CRC32 = GetCRC32_8bit_Add(inBuffer,5,0xffffffff);
    CRC32 = GetCRC32_8bit_Add(&inBuffer[5],5,CRC32);
    printf("crc32=%x\r\n",CRC32);
    
    //计算CRC16
    CRC16 = GetCRC16_8bit(inBuffer,10);
    printf("crc16=%x\r\n",CRC16);
    
    //追加方式计算CRC16
    CRC16 = GetCRC16_8bit_Add(inBuffer,5,0xffff);
    CRC16 = GetCRC16_8bit_Add(&inBuffer[5],5,CRC16);
    printf("crc16=%x\r\n",CRC16);
    
    //计算MD5
    utils_md5(inBuffer,10,output);
    printf("MD5=");
    for(i=0;i<32;i++)
    {
        printf("%c",output[i]);
    }
    
    
    //计算SHA1
    utils_sha1(inBuffer,10,output);
    printf("\r\nSHA1=");
    for(i=0;i<40;i++)
    {
        printf("%c",output[i]);
    }
    
    //计算带密钥的MD5
    utils_hmac_md5((char*)inBuffer,10,(char*)output,(char*)Key,5);
    printf("\r\nMD5=");
    for(i=0;i<32;i++)
    {
        printf("%c",output[i]);
    }
    
    
    //计算带密钥的SHA1
    utils_hmac_sha1((char*)inBuffer,10,(char*)output,(char*)Key,5);
    printf("\r\nSHA1=");
    for(i=0;i<40;i++)
    {
        printf("%c",output[i]);
    }    
}
